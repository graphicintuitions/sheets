###Sheets
Makes working with specific ranges easier in Google Sheets. See ```/src/examples``` for an example of how to use with a google spreadsheet.

In order to use this you'll need to follow the steps on this page for your sheet:
https://developers.google.com/sheets/api/quickstart/php