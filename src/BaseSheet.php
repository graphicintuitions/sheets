<?php

namespace TeamGI\Sheets;

use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_NamedRange;

abstract class BaseSheet
{
    /**
     * @var Google_Service_Sheets;
     */
    protected $service;

    /**
     * @var string The google sheet ID
     */
    protected $sheetId;

    /**
     * @var Google_Service_Sheets_NamedRange[]
     */
    protected $namedRanges = [];

    /**
     * @var array
     */
    protected $rangeData = [];
    private $clientSecretFile;
    private $credentialsFile;

    public function __construct($clientSecretFile, $credentialsFile)
    {
        $this->sheetId = static::SHEET_ID;

        $this->clientSecretFile = $clientSecretFile;
        $this->credentialsFile  = $credentialsFile;

        $client        = $this->getClient();
        $this->service = new Google_Service_Sheets($client);

        $this->setNamedRanges();
    }


    /**
     * Returns an authorized API client.
     * @return Google_Client the authorized client object
     */
    public function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Trailtech Sheets');
        $client->setScopes(Google_Service_Sheets::SPREADSHEETS_READONLY);
        $client->setAuthConfig($this->clientSecretFile);
        $client->setAccessType('offline');

        // Load previously authorized credentials from a file.
        $credentialsPath = $this->credentialsFile;
        if (file_exists($credentialsPath)) {
            $accessToken = json_decode(file_get_contents($credentialsPath), true);
        } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            printf("Open the following link in your browser:\n%s\n", $authUrl);
            print 'Enter verification code: ';
            $authCode = trim(fgets(STDIN));

            // Exchange authorization code for an access token.
            $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

            // Store the credentials to disk.
            if ( ! file_exists(dirname($credentialsPath))) {
                mkdir(dirname($credentialsPath), 0700, true);
            }
            file_put_contents($credentialsPath, json_encode($accessToken));
            printf("Credentials saved to %s\n", $credentialsPath);
        }
        $client->setAccessToken($accessToken);

        // Refresh the token if it's expired.
        if ($client->isAccessTokenExpired()) {
            $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
        }

        return $client;
    }

    /**
     * Gets a named range by name
     *
     * @param $name
     *
     * @return Google_Service_Sheets_NamedRange
     * @throws SheetException
     */
    public function getNamedRange($name)
    {
        if ( ! isset($this->getNamedRanges()[$name])) {
            throw new SheetException('That named range does not exist in this spreadsheet.');
        }

        return $this->getNamedRanges()[$name];
    }

    /**
     * @return Google_Service_Sheets_NamedRange[]
     */
    public function getNamedRanges()
    {
        return $this->namedRanges;
    }

    /**
     * Sets the named ranges in a convenient array which makes lookup easier
     */
    public function setNamedRanges()
    {
        foreach ($this->service->spreadsheets->get($this->sheetId)->getNamedRanges() as $range) {
            /** @var Google_Service_Sheets_NamedRange $range */
            $this->namedRanges[$range->getName()] = $range;
        }
    }

    /**
     * Returns a 0 based index of the column that this named range is referring to
     *
     * @param string $namedRange The named range col index you wish to fetch
     *
     * @throws SheetException
     * @return int
     */
    public function getNamedRageColIndex($namedRange)
    {
        if ( ! isset($this->getNamedRanges()[$namedRange])) {
            throw new SheetException('That named range does not exist in this spreadsheet.');
        }

        return $this->getNamedRanges()[$namedRange]->getRange()->getStartColumnIndex();
    }

    /**
     * Force refresh of all the defined ranges
     *
     * @throws SheetException
     */
    public function refreshRanges()
    {
        foreach ($this->range as $rangeName => $range) {
            $this->fetchRange($rangeName, true);
        }
    }

    /**
     * Fetch a range as defined in the protected $range property
     *
     * @param string $rangeName The name of the range
     * @param bool $forceRefresh Should we force a refresh or use the stored values?
     *
     * @return mixed
     * @throws SheetException
     */
    public function fetchRange($rangeName, $forceRefresh = false)
    {
        if (empty($this->range[$rangeName])) {
            throw new SheetException("'$rangeName' is not defined.");
        }

        if (empty($this->rangeData[$rangeName]) || $forceRefresh) {
            $this->rangeData[$rangeName] = $this->service
                ->spreadsheets_values
                ->get(static::SHEET_ID, $this->range[$rangeName]);
        }

        return $this->rangeData[$rangeName];
    }

    public function __get($name)
    {
        if ( ! array_key_exists($name, $this->range)) {
            throw new SheetException("Property $name is not defined");
        }

        return $this->fetchRange($name);
    }
}