<?php

use TeamGI\Sheets\BaseSheet;

class TodoSheet extends BaseSheet
{
    const SHEET_ID = '1J7pmLlQKWQuO5Wsrna3BG72aWiv1uT5l5-PJ6BKzTkY';

    protected $range = [
        'todos'  => 'Master Customer List!A2:B1000',
    ];
}